import { NavLink } from "react-router-dom";
import { FaShoppingCart } from "react-icons/fa";
import logoImg from "../../assets/images/logo.png";
import styles from "./Navbar.module.css";
import { useContext } from "react";
import { CartContext } from "../context/cart/CartContext";
const Navbar = (props) => {
  const {itemAmount} = useContext(CartContext);


  const onSearchChange=($e)=>{
    props.onSearchChange($e.target.value);
  }
    return ( 
      <nav className={`navbar navbar-expand-lg bg-body-tertiary  ${styles.topnav}`}  >
      <div className="container-fluid">
        <a className="navbar-brand" href="#">
        <img src={logoImg} height={50} alt="logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0" >
            <li className="nav-item">
              <NavLink to="/" className="nav-link active" aria-current="page"style={{ color: "white" }}  >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/products" className= "nav-link" aria-current="page" style={{ color: "white" }} >
                Products
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="/about" className="nav-link"style={{ color: "white" }} >
                Aboutus
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink to="#" className="nav-link" style={{ color: "white" }}>
                Condact us
              </NavLink>
            </li>
           
            {/* <li className="nav-item">
              <NavLink to="#" className="nav-link "style={{ color: "white" }}  >
                
                 <FaShoppingCart />
              </NavLink>
            </li> */}

          </ul>
          <form className="d-flex" role="search">
            <input
              className="form-control me-2"
              type="search"
              placeholder="Search"
              aria-label="Search"
              onChange={onSearchChange}
            />
            <button className="btn btn-outline-success" style={{backgroundColor:'#',color:'white'}}  type="submit">
              Search
            </button>
          </form>
          <NavLink to="/cart" className="nav-link  ms-4 me-4 d-flex"style={{ color: "white" }}  >
                
                <FaShoppingCart />
                <div className="cart-amt " style={{ color: "white" , marginTop: "-20px" ,fontSize : "14px"}}>({itemAmount})</div>
             </NavLink>
        </div>
      </div>
    </nav>
             
     );
}

export default Navbar;