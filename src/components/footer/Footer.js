import styles from "./Footer.module.css";
function Footer() {
    return (
      //   <footer className={styles.footerContainer} >
      //   © Copyright 2021 Kiebot. All Rights Reserved
      // </footer>
  
      <footer className="text-center text-white">
        <div className="container">
          <section className="my-3">
            <div className="row text-center d-flex justify-content-center pt-5">
              <div className="col-md-2">
                <h6 className="text-uppercase">
                  <a href="#!" className="text-white">
                    About us
                  </a>
                </h6>
              </div>
  
              <div className="col-md-2">
                <h6 className="text-uppercase">
                  <a href="#!" className="text-white">
                    Products
                  </a>
                </h6>
              </div>
  
              <div className="col-md-2">
                <h6 className="text-uppercase">
                  <a href="#!" className="text-white">
                    Help
                  </a>
                </h6>
              </div>
  
              <div className="col-md-2">
                <h6 className="text-uppercase">
                  <a href="#!" className="text-white">
                    Contact us
                  </a>
                </h6>
              </div>
            </div>
          </section>
  
          <hr
            className="my-2"
            style={{ backgroundColor: "rgb(222 227 233 / 25%)" }}
          />
        </div>
  
        <div className="text-center p-0">
          © Copyright 2021 Online Cart. All Rights Reserved
        </div>
      </footer>
    );
  }

export default Footer;