import { createContext, useState ,useEffect} from "react";


export const CartContext = createContext();

const CartProvider = ({children}) => {
const [cart, setCart] = useState([])
const [itemAmount, setItemAmount] = useState(0)
const [totalPrice, setTotalPrice] = useState(0)

  useEffect(() => {
    if(cart) {
      const amount = cart.reduce((accumulator,currentItem) => {
        return accumulator + currentItem.amount
      },0)
      setItemAmount(amount)
    }
  }, [cart])
  
  useEffect(() => {
      const total = cart.reduce((accumulator,currentItem) => {
        return accumulator + currentItem.price * currentItem.amount
      },0)
      setTotalPrice(total)
    })

const addToCart = (prod,id) => {
  const newItem = {...prod, amount: 1}
  const cartItem = cart.find((item) => {
    return item.id === id;
  });
  if (cartItem) {
    const newCart = [...cart].map((item) => {
      if (item.id === id) {
        return {
          ...item,
          amount: cartItem.amount + 1,
        };
      } else {
        return item;
      }
    });
    setCart(newCart);
  } else {
    setCart([...cart, newItem]);
  }
}
console.log(cart);
const emptyCart =() => {
  setCart ([])
}
const removeFromCart = (id) => {
  const newCart = cart.filter((item) =>{
    return item.id !==id;
  });
  setCart(newCart)
};
const increaseAmount = (id) => {
  const cartItem = cart.find((item) => item.id === id);
  addToCart(cartItem,id);
}

const decreaseAmount = (id) => {
  const cartItem = cart.find((item) => item.id === id);
  if (cartItem) {
    const newCart = cart.map((item) => {
      if (item.id === id) {
        return { ...item, amount: cartItem.amount - 1 };
      } else {
        return item;
      }
    });
    setCart(newCart);
  }
  if (cartItem.amount < 2) {
    removeFromCart(id);
  }
};
  return (
    <CartContext.Provider value={{addToCart ,cart, emptyCart, removeFromCart, increaseAmount,decreaseAmount,itemAmount,totalPrice}}>{children}</CartContext.Provider>
  )
};
export default CartProvider;
