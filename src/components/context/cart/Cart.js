import { useContext } from "react";
import "./Cart.css";
import { Link } from "react-router-dom";
import { CartContext } from "./CartContext";
import { FaMinus, FaPlus } from "react-icons/fa";
import { MdDeleteForever } from "react-icons/md";


const Cart = () => {
  const { cart, emptyCart,removeFromCart, increaseAmount ,decreaseAmount ,itemAmount,totalPrice} = useContext(CartContext);

  return (
    <section className="shopping-cart">
      <div className="card  mt-5">
        <div className="row">
          <div className="col-md-8 cart">
            <div className="title">
              <div className="row">
                <div className="col">
                  <h4>
                    <b>Shopping Cart</b>
                  </h4>
                </div>
                <div className="col align-self-center text-right text-muted">
                  {itemAmount} items
                </div>
              </div>
            </div>

            {cart.map((item) => {
              return (
                <div className="row border-top border-bottom">
                  <div className="row main align-items-center">
                    <div className="col-2">
                    <Link to = {`/products/${item.id}/${item.title}`}>
                      <img className="img-fluid" src={item.image} />
                      </Link>
                    </div>
                    <div className="col">
                    <Link to = {`/products/${item.id}/${item.title}`}>
                      <div className="row text-muted">{item.title}</div>
                      </Link>
                    </div>
                    <div className="col">${item.price}</div>
                    <div className="col">
                        <FaPlus onClick={()=>increaseAmount(item.id)} size={10} className="me-2"/>
                        <sspan>{item.amount}</sspan>
                        <FaMinus onClick={()=>decreaseAmount(item.id)} size={10} className="ms-2"/>
                    </div>
                    <div className="col">${item.price * item.amount}</div>
                    <div className="col">
                    <MdDeleteForever
                        size={20}
                        color="red"
                        onClick={()=>removeFromCart(item.id)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
            <div className="d-flex justify-content-around">
              <div className="back-to-shop">
                <Link  to = {"/"}>
                <span className="text-muted">Back to shop</span>
                </Link>
              </div>
              <div onClick={emptyCart}>Remove All </div>
            </div>
          </div>
          <div className="col-md-4 summary">
            <div>
              <h5>
                <b>Summary</b>
              </h5>
            </div>
            <hr />
            <div className="row">
              <div className="col" style={{ paddingLeft: 0 }}>
                ITEMS : {itemAmount}
              </div>
              <div className="col text-right">$ {parseFloat(totalPrice).toFixed(2)}</div>
            </div>
            <form>
              <p>SHIPPING</p>
              <select>
                <option className="text-muted">Standard-Delivery- $ 5.00</option>
              </select>
              <p>GIVE CODE</p>
              <input id="code" placeholder="Enter your code" />
            </form>
            <div
              className="row"
              style={{
                borderTop: "1px solid rgba(0,0,0,.1)",
                padding: "2vh 0",
              }}
            >
              <div className="col">TOTAL PRICE</div>
              <div className="col text-right">
              {totalPrice === 0 ? (
                `$ 0`
              ) : (
                  `$ ${parseFloat(totalPrice + 5).toFixed(2)}`
              )}
              </div>
            </div>
            <button className="btn">CHECKOUT</button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Cart;
