import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { CartContext } from "../context/cart/CartContext";
const ProductDetail = () => {
  const { addToCart } = useContext(CartContext);
  const params = useParams();
  const prodId = params.productId;
  const [product, setProduct] = useState({});

  function loadProductById() {
    fetch("https://fakestoreapi.com/products/" + prodId)
      .then((response) => {
        response
          .json()
          .then((data) => {
            setProduct(data);
          })
          .catch((error) => {
            console.log(error);
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }
  useEffect(() => {
    loadProductById();
  }, [prodId]);

  console.log(product);
  return (
    <>
      <div className="container-fluid row m-5">
        <div
          className="card m-5 col-sm-12 col-md-6"
          style={{ width: "370px" }}
          key={product.id}
        >
          <img
            className="card-img p-3"
            src={product.image}
            height={410}
            alt="Card image"
          />
        </div>

        <div className="col-sm-12 col-md-6 m-5">
          <div>
            <h2 className="card-title"> {product.title}</h2>
          </div>
          <div className="mt-3">
            <h4>{product.category}</h4>
          </div>
          <div className="mt-3">
            <h6>{product.description}</h6>
          </div>
          <div className="mt-3">
            <h5>{product.price}</h5>
          </div>
          <div>
            {/* <span>
               {'Rating:'}{product.rating.rate}
               {'Count:'}{product.rating.count}
           </span> */}
          </div>
          <button onClick={() => addToCart(product, product.id)}>
            Add to cart
          </button>
        </div>
      </div>
    </>
  );
};

export default ProductDetail;

{
  /* <h1>This is productdetail</h1>
     <h3>params: {JSON.stringify(params) }</h3>
     <h3>{[product.title]}</h3> */
}
