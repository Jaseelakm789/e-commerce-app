import { useState, useEffect } from "react";
import ProductCard from "./ProductCard";
import styles from "./ProductList.module.css";
import Hero from "../hero/Hero";
const ProductList = (props) => {
    const searchText=props.searchText;
    const [ProductList, setproductList] = useState([]);

    function loadproductbySearchText() {
      // console.log('loadproductbySearchText');
      fetch('https://fakestoreapi.com/products').then((response) => {
           response. json().then((data) => {
              setproductList(data);
          })
      })
   }

   useEffect(()=> {
    loadproductbySearchText();

    },[searchText]);

   //const filteredProducts=props.products.filter((p)=>{
     // return p.title.toLowerCase().includes(searchText.toLowerCase());
   // });
    return (
      <>
    
      <h1 className="text-center mt-4 fond-bold">Products</h1>
      <section className={styles.productItems} id="list">
      
        {/* {ProductList.map((p) => {
          return <ProductCard product={p} />;
        })} */}
        <ProductCard ProductList = {ProductList}/>
     
      </section>
      </>
    );
  }
  
  export default ProductList;
  