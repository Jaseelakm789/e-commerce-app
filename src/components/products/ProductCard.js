import { Link } from "react-router-dom";
import styles from "./ProductCard.module.css";
import { CartContext } from "../context/cart/CartContext";
import { useContext } from "react";
function ProductCard({ProductList}) {
    // console.log("Product:",props);
    // var prod=props.product;
  const {addToCart} = useContext(CartContext)
  return (

  // <div className="card m-1" style={{width:'280px'}}>
  //   <img className="card-img p-3"  
  //   src={prod.image} height={300} alt="Card image"/>
  //   <div className="card-body">
  //     <h6 className="card-title">{prod.title}</h6>
  //     <div 
  //        className={styles.productPrice}>{'Price:$'} {prod.price}
  //     </div>
     
      
  //     <a href="#" className="btn btn-primary ms-5 mt-3">More..</a>
  //   </div>
  // </div>

  ProductList.map((prod) => (
  <div className="card m-3" style={{width:'230px'}} key={prod.id}>
    <img className="card-img p-3"  
    src={prod.image} height={210} alt="Card image"/>
    <div className="card-body">
      <Link to={`/products/${prod.id}/${prod.title}`} >
      <h6 className="card-title">{prod.title}</h6>
      </Link>
      <div 
         className={styles.productPrice}>{'Price:$'} {prod.price}
      </div>
      <button onClick={() => addToCart(prod, prod.id)} className="btn btn-primary mt-3">Add To Cart </button>
    </div>
  </div>
  
  ))

  );
}

export default ProductCard;
