import logo from './logo.svg';
import './App.css';
import ProductList from './components/products/ProductList';
import Footer from './components/footer/Footer';
import Navbar from './components/navbar/Navbar';
import { useState, createContext } from 'react';
import Banner from './components/banner/Banner';
import {Route, Routes} from 'react-router-dom';
import Home from './components/home/Home';
import Hero from './components/hero/Hero';
import Aboutus from './components/screens/about';
import Product from './components/productdetail/ProductDetail';
import ProductDetail from './components/productdetail/ProductDetail';
import Cart from './components/context/cart/Cart';

function App() {
 
  const [searchText,setSearchText]=useState('');
  function onSearchChange(text){
    setSearchText(text);
  }


  return (
    <div className="container-fluid">

    <Navbar onSearchChange={onSearchChange}  />
      <Routes>
      <Route path="/products" element= {<ProductList />} /> 
      <Route path="/" element= {<Home />} />
      <Route path="/about" element= {<Aboutus/>} />
      <Route path="/cart" element= {<Cart/>} />
      <Route path="/products/:productId/:productName" element= {<ProductDetail />} />
      </Routes>
      
    <Footer />
  </div>
  );
}

export default App;
//<ProductList searchText={searchText}   />









